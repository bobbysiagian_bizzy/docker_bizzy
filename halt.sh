#!/bin/bash

printf "${GREEN}"
echo "---------------------------------------------"
echo "--------- Dockerize Bizzy Indonesia ---------"
echo "---------------------------------------------"
printf "${NC}"

docker stop $(docker ps -q)

printf "${GREEN}"
echo "----------- Machines has stopped. -----------"
printf "${NC}"
