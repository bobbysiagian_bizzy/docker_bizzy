#!/bin/bash

printf "${GREEN}"
echo "---------------------------------------------"
echo "--------- Dockerize Bizzy Indonesia ---------"
echo "---------------------------------------------"
printf "${NC}"

docker-compose up -d

printf "${GREEN}"
echo "--------- Machine is on running now. --------"
printf "${NC}"
